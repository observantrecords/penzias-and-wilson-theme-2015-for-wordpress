# Penzias and Wilson 2015 Theme

A custom theme for [Penzias and Wilson](http://penziasandwilson.com/).

## Dependencies

* [Observant Records 2015 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2015-for-wordpress)
